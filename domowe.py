class Plants(object):
        pass
plants = Plants()

spruce = Plants()
maple = Plants()
grass = Plants()

print(plants)
print(isinstance(plants, Plants))
print(isinstance(plants, object))
print(spruce)
print(maple)
print(grass)

print(isinstance(spruce, Plants))
print(isinstance(maple, Plants))
print(isinstance(grass, Plants))
print(isinstance(grass, object))

#2 zadanie

class Cosmetics(object):
        pass
cosmetics = Cosmetics()

shower_gel = Cosmetics()
deodorant = Cosmetics()
perfume = Cosmetics()

print(cosmetics)
print(isinstance(cosmetics, Cosmetics))
print(isinstance(cosmetics, object))
print(shower_gel)
print(deodorant)
print(perfume)

print(isinstance(shower_gel, Cosmetics))
print(isinstance(deodorant, Cosmetics))
print(isinstance(perfume, Cosmetics))
print(isinstance(shower_gel, object))
print(isinstance(deodorant, Plants))

#3 zadanie

class Baking(object):
        pass
baking = Baking()

cake = Baking()
cookies = Baking()
layer_cake = Baking()

print(baking)
print(isinstance(baking, Baking))
print(isinstance(baking, object))
print(cake)
print(cookies)
print(layer_cake)

print(isinstance(cake, Baking))
print(isinstance(cookies, Baking))
print(isinstance(layer_cake, Baking))
print(isinstance(cookies, object))
print(isinstance(layer_cake, Cosmetics))

#4 zadanie

class Netflix(object):
        pass
netflix = Netflix()

class Movies(Netflix):
        pass

class TV_series(Netflix):
        pass

The_100 = TV_series()
Ted = Movies()
Bright = Movies()
Daredevil = TV_series()


print(netflix)
print(isinstance(netflix, Netflix))
print(isinstance(netflix, object))
print(Ted)
print(The_100)
print(Daredevil)
print(Bright)


print(isinstance(The_100, TV_series))
print(isinstance(Bright, Movies))

print(isinstance(Daredevil, Movies))
print(isinstance(Ted, TV_series))

print(isinstance(The_100, object))
print(isinstance(Ted, Netflix))

